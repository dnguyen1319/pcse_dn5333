#include "mpi.h"
#include <stdio.h>

int main(int argc, char *argv[]) {

int ierr, tasks, rank, len, numtasks, tag1, tag2, dest, src;
double time1, time2;
char inchar = 'b', outchar = 'a';

	ierr = MPI_Init(&argc, &argv);
	if (ierr != MPI_SUCCESS) {
	printf("Error starting MPI program, terminating.\n");
	MPI_Abort(MPI_COMM_WORLD, ierr);
	}
	
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	
	if (numtasks != 2) {
	printf("Too many processes. This program will only work with 2.\n");
	printf("There are currently %d process(es). Terminating.\n", numtasks);
	MPI_Abort(MPI_COMM_WORLD, 0);
	}
	
	char name[MPI_MAX_PROCESSOR_NAME];
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(name, &len);
	MPI_Status Stat;
	printf("Tasks available: %d. Printing from: %d \n", numtasks, rank);
	tag1 = 0;
	tag2 = 1;

	printf("Before message pass: inchar %c - using task %d.\n", inchar, rank);
	printf("Before message pass: outchar: %c - Using task %d.\n", outchar, rank);

	if (rank == 0){
	  dest = 1;
	  src = 1;
	  MPI_Send(&outchar, 1, MPI_CHAR, dest, tag1, MPI_COMM_WORLD);
 	  MPI_Recv(&inchar, 1, MPI_CHAR, src, tag2, MPI_COMM_WORLD, &Stat);
	 }
	else if (rank == 1){
	  dest = 0;
	  src = 0;
	  MPI_Recv(&inchar, 1, MPI_CHAR, src, tag1, MPI_COMM_WORLD, &Stat);
	  MPI_Send(&outchar, 1, MPI_CHAR, dest, tag2, MPI_COMM_WORLD);
	 }

	printf("After message pass: inchar: %c - Using task %d.\n", inchar, rank); 	
	printf("After message pass: outchar: %c - Using task %d.\n", outchar, rank);
	printf("Tag ID: %d Source ID:%d \n", Stat.MPI_SOURCE, Stat.MPI_TAG);

	MPI_Finalize();

return 0;

}
