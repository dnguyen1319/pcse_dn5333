#include <stdio.h>
#include "mpi.h"
#define NPROCS  4
#define ASIZE   8
#define ASIZEPP (ASIZE / NPROCS)
#define ROOT    2
#define SOME_MAX 8
int main(int argc, char *argv[]){

double master_vec[ASIZE], local_vec[ASIZEPP];
int i, j, rank, numprocs;
int sendcount[NPROCS], recvcount[NPROCS], displacement[NPROCS];
double root_sum, local_sum, global_sum;


  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == ROOT)
   {
     for (i=0; i<ASIZE; i++){
    master_vec[i] = 100;}
   }
  if (rank != ROOT)
  {
   for (i=0;i<ASIZE;i++){
   master_vec[i] = -1;
   }
  }

  for (i=0; i<NPROCS; i++){
  sendcount[i] = 2;
  recvcount[i] = 2;
  displacement[i] = 2*i; 
  }   

  MPI_Scatterv(&master_vec[0], sendcount, displacement, MPI_DOUBLE, &local_vec[0], 2, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
  
  for (i=0; i<ASIZEPP; i++){
  local_vec[i] = rank + local_vec[i];}

  for(i=0; i<numprocs; i++){
    if(rank == i){
      printf("***(Rank %i): local_vec Output:\n",rank);
      for(j = 0; j < ASIZEPP; j++){
        printf("(Rank %i): %i %f\n",rank,j,local_vec[j]);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
 
  for (i=0; i< ASIZEPP; i++){
  local_sum += local_vec[i];
  }

  MPI_Allreduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Gatherv(&local_vec[0], 2, MPI_DOUBLE, &master_vec[0], recvcount, displacement, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
  
  if (rank == ROOT){
    for (i=0; i<ASIZE; i++){
    root_sum += master_vec[i];
    }

  }

 for(i=0; i<numprocs; i++){
  if(rank==i){
  printf("***(Rank %i): master_vec Output:\n", rank);
   for(j = 0; j < ASIZE; j++){
    printf("(Rank %i): %i %f\n",rank,j,master_vec[j]);
    }
   printf("(Rank %i): The ROOT process is: %i\n", rank, ROOT);
   printf("(Rank %i): Root            Sum: %f\n", rank, root_sum);
   printf("(Rank %i): MPI_Allreduce      Sum: %f\n", rank, global_sum);
   }
   MPI_Barrier(MPI_COMM_WORLD);
  }

 MPI_Finalize();
return 0;
}
