#include <stdio.h>
#include "mpi.h"
#define NPROCS  4
#define ASIZE   8
#define ASIZEPP (ASIZE / NPROCS)
#define ROOT    2
#define SOME_MAX 8
int main(int argc, char *argv[]) {
  double master_vec[ASIZE], local_vec[ASIZEPP];
  int i, j, rank, numprocs;
  double root_sum, local_sum, global_sum;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Request reqs[SOME_MAX];
  MPI_Status stats[SOME_MAX];

  if (rank == ROOT)
   {
     for (i=0; i<ASIZE; i++){
    master_vec[i] = 100;}
   }
  if (rank != ROOT)
  {
   for (i=0;i<ASIZE;i++){
   master_vec[i] = -1;
   }
  }
   
  MPI_Scatter(&master_vec[0], 2, MPI_DOUBLE, &local_vec[0], 2, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
  
  for (i=0; i<ASIZEPP; i++){
  local_vec[i] = rank + local_vec[i];}

  for(i=0; i<numprocs; i++){
    if(rank == i){
      printf("***(Rank %i): local_vec Output:\n",rank);
      for(j = 0; j < ASIZEPP; j++){
        printf("(Rank %i): %i %f\n",rank,j,local_vec[j]);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
 
  for (i=0; i< ASIZEPP; i++){
  local_sum += local_vec[i];
  }

  MPI_Reduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);
  MPI_Gather(&local_vec[0], 2, MPI_DOUBLE, &master_vec[0], 2, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
  
  if (rank == ROOT){
    for (i=0; i<ASIZE; i++){
    root_sum += master_vec[i];
    }

  }

 for(i=0; i<numprocs; i++){
  if(rank==i){
  printf("***(Rank %i): master_vec Output:\n", rank);
   for(j = 0; j < ASIZE; j++){
    printf("(Rank %i): %i %f\n",rank,j,master_vec[j]);
    }
   printf("(Rank %i): The ROOT process is: %i\n", rank, ROOT);
   printf("(Rank %i): Root            Sum: %f\n", rank, root_sum);
   printf("(Rank %i): MPI_Reduce      Sum: %f\n", rank, global_sum);
   }
   MPI_Barrier(MPI_COMM_WORLD);
  }

 MPI_Finalize();
return 0;
}
