#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

int main(int argc,char **arg) {

  int nsteps=1000000000; // that's one billion
  double tstart,tend,elapsed, pi,quarterpi,h, pi_num;
  tstart = omp_get_wtime(); //gettime();
  pi = 0.; h = 1./nsteps;

#pragma omp parallel
{
  int i, j;
  int range, thread_id, first_step, last_step, num_threads;
  num_threads = omp_get_num_threads();
  range = nsteps/num_threads;
  thread_id = omp_get_thread_num();
  first_step = thread_id * range;
  last_step = (thread_id + 1) * range;

for (i=first_step; i <= last_step; i++) {
    double
      x = i*h,
      y = sqrt(1-x*x);
      quarterpi += h*y;
  }
}

  pi = 4*quarterpi;
  tend = omp_get_wtime(); //gettime();
  elapsed = tend-tstart;

  printf("Computed pi=%e in %6.3f seconds\n",pi,elapsed);


 return 0;
}

