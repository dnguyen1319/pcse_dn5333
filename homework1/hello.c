#include <stdlib.h>
#include <stdio.h>
#include <omp.h>


int main(int argc,char **argv) {
    {
    int thread_id, num_threads;
    num_threads = omp_get_num_threads();
    thread_id = omp_get_thread_num();
    if (omp_in_parallel()==0) 
    { 
    printf("Outside a parallel region, hello from from thread %d out of %d\n", thread_id, num_threads);
    } 
    else
    {
    printf("Inside a parallel region, hello from thread %d out of %d\n", thread_id, num_threads);
    }
    }

#pragma omp parallel
  {
    int thread_id, num_threads;
    num_threads = omp_get_num_threads();
    thread_id = omp_get_thread_num();
    if (omp_in_parallel()==0) 
   { printf("Outside a parallel region, hello from from thread %d out of %d\n", thread_id, num_threads);
   } else
    {
     printf("Inside a parallel region, hello from thread %d out of %d\n", thread_id, num_threads);
    }
  }
   
   int get_processors;
   get_processors = omp_get_num_procs();
   printf("The number of threads on this hardware is %d\n", get_processors);
  
  return 0;
}
