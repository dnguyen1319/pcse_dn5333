As a precursor, hello.c will apply to number 1-4.

1. A simple hello.c with the following code:

#include <stdlib.h>
#include <stdio.h>

int main(int argc,char **argv) {

#pragma omp parallel
  {
    printf("Hello world!\n");
  }

  return 0;
}

Will simply output "Hello world!".

2. By modifying the number of threads using "export OMP_NUM_THREADS=(integer)"
the output (if compiled using -openmp) "Hello World!" is seen as many times as  there are "integer" threads that were set.

Thus "export OMP_NUM_THREADS=4" will have the program output "Hello World!"
4 times, as the command goes through each available thread and output the same instruction - in this case, the printf instruction.

3. The code has been modified in accordance to the instructions:

#pragma omp parallel
  {
      int thread_id, num_threads;
      num_threads = omp_get_num_threads();
      thread_id = omp_get_thread_num();
      printf("Hello from from thread %d out of %d\n", thread_id, num_threads);
  }

In this format, num_threads is declared and receives the value of the number of threads available for usage, along with
thread_id being declared and receiving a value of the thread ID that is being used.

Outputting this with two cores (again, compiling with the intel compilier and an instruction to use with OpenMP) will output:
"Hello from thread 0 out of 2"
"Hello from thread 1 out of 2"

When outputting with more than 2 cores such as 16 cores, the thread ID may be out of order for the output. A possible reason
for this is that some threads reach completion before the other threads causing the output value to appear out of order.

If these clauses are declared outside of the parallel region, such as above #pragma omp parallel,
the values that slide into the declared variables will be that of a single thread as it is still part of the "Sequential"
part of the code, and lays outside of the parallel region where there exists multiple threads.

4. The code is re-written, all within the same main() function:


int main(int argc,char **argv) {
    {
    int thread_id, num_threads;
    num_threads = omp_get_num_threads();
    thread_id = omp_get_thread_num();
    if (omp_in_parallel()==0)
    {
    printf("Outside a parallel region, hello from from thread %d out of %d\n", thread_id, num_threads);
    }
    else
    {
    printf("Inside a parallel region, hello from thread %d out of %d\n", thread_id, num_threads);
    }
    }

#pragma omp parallel
  {
    int thread_id, num_threads;
    num_threads = omp_get_num_threads();
    thread_id = omp_get_thread_num();
    if (omp_in_parallel()==0)
   { printf("Outside a parallel region, hello from from thread %d out of %d\n", thread_id, num_threads);
   } else
    {
     printf("Inside a parallel region, hello from thread %d out of %d\n", thread_id, num_threads);
    }

   }

omp_in_parallel() in this situation receives a value of either 0 or a nonzero value. If the value received is == 0,
the region is not inside a parallel region. If the value received != 0, the instruction is within a parallel region.

The output in this code appears as follows with two cores:

"Outside a parallel region, hello from thread 0 out of 1"
"Inside a parallel region, hello from thread 0 out of 2"
"Inside a parallel region, hello from thread 1 out of 2"

How exciting!

5. We amend the code by adding the following (Alternatively as I found out, I simply made a new code "five.c" with only the following):

  int get_processors;
   get_processors = omp_get_num_procs();
   printf("The number of threads on this hardware is %d\n", get_processors);

When the code is compiled using "icc -openmp -o" instructions, the output is 16 cores. This is because this is the
maximum amount of threads possible using only the local processor's technology. No further instructions have been given
to use additional resources such as co-processors, or nodes outside this one.

6. Even using the "export OMP_NUM_THREADS=(int)" command, the omp_get_num_procs value cannot be changed. The instruction asks how many thread
 can be supported as it is currently compiled - that number is 16.

Placed inside or outside the parallel region does not matter.

7. Amending our code such that we add -mmic to the compilation, we have "icc -openmp -mmic -o" as the instruction to compile.
As said in the instruction, this will have the program execute on the Intel Xeon Phi co-processor. We get a whopping 244 cores.

8. See "eight.c"

As per instructions, a program was written such that a subfunction had a non parallel region and a parallel region, and
inside the main function was a non-parallel region and a parallel region.
For the sake of identification, a message would read inside the subfunction "Inside subfunction's parallel" and "Outside subfunction's parallel".
This subfunction would be called in the main function twice: once outside the parallel region, and then once inside the parallel region. 

As we are only using four threads, -mmic was not necessary for compilation instructions.
The expected result would be to receive 25 messages total. 1 Outside, 4 inside, followed by the parallel region processing 4 outsides and 16
insides.

The code was compiled, and the run produced the following results:

1x Outside
4x Inside

And then, sometimes in mixed order, but consistently:
4x Outside
4x Inside

With only 13 messages, this was not in line with expectations of how parallel code works. This is explained in 9.

9. "OpenMP Parallelism is controlled by the environment variable OMP_NESTED."
After setting OMP_NESTED=TRUE in the environment, the following result was achieved.

1x outside
4x inside

And then:
4x outside
16x inside

Which follows the expectations of how parallelism should be functioning.
