#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void parallelish(int x) {
printf("Outside subfunction's paralell\n");

#pragma omp parallel
{
printf("Inside subfunction's parallel\n");
}
}


int main(char **argv){
parallelish(0);

#pragma omp parallel
{
parallelish(0);
}
return 0;

}
