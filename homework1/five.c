#include <stdlib.h>
#include <stdio.h>

int main(int argc,char **argv) {

  int get_processors;
   get_processors = omp_get_num_procs();
   printf("The number of threads on this hardware is %d\n", get_processors);

  return 0;
}
