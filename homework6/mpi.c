#include "mpi.h"
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

int ierr, tasks, rank, len, numtasks;
double time1, time2;
 

	ierr = MPI_Init(&argc, &argv);
	if (ierr != MPI_SUCCESS) {
	printf ("Error starting MPI program, terminating.\n");
	MPI_Abort(MPI_COMM_WORLD, ierr);
	}
	
	char name[MPI_MAX_PROCESSOR_NAME];
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(name, &len);

	printf("Tasks available: %d \n", numtasks);
	printf("Rank number %d \n", rank);
	
	time1 = MPI_Wtime();
	sleep(rank);	
	time2 = MPI_Wtime();
	
	printf("Rank %d Time Elapsed: %f \n", rank, time2-time1); 
	printf("Using %s\n", name);

	MPI_Finalize();

return 0;

}
